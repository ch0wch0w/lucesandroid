package com.example.juegoluces.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.juegoluces.Bombilla;
import com.example.juegoluces.Tablero;
import com.example.juegoluces.BoardLayout;
import com.example.juegoluces.LSerializer;
import com.example.juegoluces.R;

public class LightsOutPlay extends Activity {
  
  public static final String NEW_GAME = "com.example.juegoluces.NEW_GAME";
  
  
  private Tablero gameBoard = null;
  private LSerializer gameBoardSerializer;
  private Dialog levelWonDialog;
  private boolean isNewGame = true;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.game_play);
    
    this.gameBoardSerializer = new LSerializer(this);
    
    
    
  }
  
  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    

  }
  
  @Override
  protected void onPause() {
    super.onPause();
    
    if (gameBoard != null) {
      gameBoard.stopTimer();
      gameBoardSerializer.serialize(gameBoard);
    }
  }
  
  @Override
  protected void onResume() {
    super.onResume();

    if (isNewGame) {
      this.gameBoard = new Tablero(this);
      isNewGame = false;
    } else {
      this.gameBoard = gameBoardSerializer.deserialize();
    }
    if (gameBoard.testWin()) {
      updateMoveCount();
      updateSeconds();
      levelWon();
    } else {
      this.gameBoard.startPlaying();
    }
  }
  
  public void playLevel(int level) {
    int size = this.gameBoard.getSize();
    
    TextView textView = (TextView) findViewById(R.id.level_header);
    textView.setText(getString(R.string.level) + ": " + (level + 1));
    
    LinearLayout boardHolder = (LinearLayout) findViewById(R.id.board_holder);
    boardHolder.removeAllViews();
    
    BoardLayout gameBoardLayout = new BoardLayout(this, size);
    gameBoardLayout.setLayoutParams(
        new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
   
    boardHolder.addView(gameBoardLayout);
    
    updateMoveCount();
    updateSeconds();
    
    for (int i = 0; i < size * size; i++) {
      Bombilla gamePiece = gameBoard.getGamePieceByIndex(i);
      gameBoardLayout.addView(gamePiece);
    }
  }
  
  public void levelWon() {
    if (levelWonDialog != null && levelWonDialog.isShowing()) { 
      return;
    }
    
    this.levelWonDialog = new Dialog(this);
    levelWonDialog.setContentView(R.layout.win_dialog);
    
    TextView levelTimeTextView = (TextView) levelWonDialog.findViewById(R.id.level_time_text);
    TextView levelMovesTextView = (TextView) levelWonDialog.findViewById(R.id.level_moves_text);
    TextView totalTimeTextView = (TextView) levelWonDialog.findViewById(R.id.total_time_text);
    TextView totalMovesTextView = (TextView) levelWonDialog.findViewById(R.id.total_moves_text);
    levelTimeTextView.setText(this.gameBoard.getLevelSeconds() + "");
    levelMovesTextView.setText(this.gameBoard.getLevelMoves() + "");
    totalTimeTextView.setText(this.gameBoard.getTotalSeconds() + "");
    totalMovesTextView.setText(this.gameBoard.getTotalMoves() + "");
    
    Button button = (Button) levelWonDialog.findViewById(R.id.dialog_button);
    if (gameBoard.getLevel() < Tablero.LAST_LEVEL) {
      button.setText(getString(R.string.next_level).replaceFirst("%n", (gameBoard.getLevel() + 2) + ""));
      levelWonDialog.setTitle(getString(R.string.level_passed).replaceFirst("%n", (gameBoard.getLevel() + 1) + ""));
    } else {
      button.setText(R.string.ok);
      levelWonDialog.setTitle(R.string.game_over);
    }
    
    button.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        levelWonDialog.dismiss();
      }
    });
    levelWonDialog.setOnDismissListener(new OnDismissListener() {
      public void onDismiss(DialogInterface dialogInterface) {
        if (gameBoard.getLevel() < Tablero.LAST_LEVEL) {
          gameBoard.playNextLevel();
        } else {
          EditText highScoreEditText = (EditText) levelWonDialog.findViewById(R.id.high_score_name);
          gameOver(highScoreEditText.getText().toString());
        }
      }
    });
    
    levelWonDialog.show();
  }
  
  public void gameOver(String name) {
    gameBoardSerializer.serialize(null);
    this.gameBoard = null;
  }
  
  
  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    super.onPrepareOptionsMenu(menu);
    menu.clear();
    return true;
  }
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    return super.onOptionsItemSelected(item);
  }
  

  
  public void updateMoveCount() {
    TextView textView = (TextView) findViewById(R.id.moves_header);
    textView.setText(getString(R.string.moves) + ": " + this.gameBoard.getLevelMoves());
  }
  
  public void updateSeconds() {
    TextView textView = (TextView) findViewById(R.id.time_header);
    textView.setText(getString(R.string.time) + ": " + this.gameBoard.getLevelSeconds());
  }

  public void showLevelStartMessage(int level) {
    if (level == 0) {
      Toast toast = Toast.makeText(this, 
          getString(R.string.level_start_message), 
          Toast.LENGTH_LONG);
      toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 0);
      toast.show();
    }
  }
}
