package com.example.juegoluces.ui;

import java.io.FileInputStream;
import java.io.IOException;

import com.example.juegoluces.LSerializer;
import com.example.juegoluces.R;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends FragmentActivity {
	public static final int NEW_GAME_DIALOG = 1;
	
	private static final int PLAY_GAME = 1;

	
	private Button continueGameButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
			
		setContentView(R.layout.activity_main);
		
		Button newGameButton = (Button) findViewById(R.id.new_game_button);
		Button exitGameButton = (Button) findViewById(R.id.exit_game_button);
		this.continueGameButton = (Button) findViewById(R.id.continue_game_button);
		
		final Intent intent = new Intent(this, LightsOutPlay.class);
		newGameButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				//intent.putExtra(LightsOutPlay.NEW_GAME, true);
				newGame();
			}
		});
		
		if (doesSavedGameExist()) {
			continueGameButton.setEnabled(true);
		}
		
		continueGameButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				intent.putExtra(LightsOutPlay.NEW_GAME, false);
				startActivityForResult(intent, PLAY_GAME);
			}
		});
		
		exitGameButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
						finish();
						System.exit(0);
				}
		});
	
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == NEW_GAME_DIALOG) {
			return new AlertDialog.Builder(this).setMessage(R.string.erase_game)
					.setPositiveButton(R.string.ok, new OnClickListener(){
				public void onClick(DialogInterface dialog, int which) {
					startNewGame();
				}})
				.setNegativeButton(R.string.cancel, null)
				.setTitle(R.string.ok_to_erase)
				.create();
		}
		return null;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 0, R.string.instructions);
		menu.add(0, 2, 0, R.string.info);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
        case 1: //instrucciones
        	showInstructionsDialog();
            return true;
        case 2: //info
        	showAboutDialog();
            return true;
        default:
            return super.onOptionsItemSelected(item);
		}
		
	}
	
	private void newGame() {
		//if (doesSavedGameExist()) {
		//	showDialog(NEW_GAME_DIALOG);
		//} else {
			startNewGame();
		//}
	}
	
	private void startNewGame() {
		Intent intent = new Intent(this, LightsOutPlay.class);
		intent.putExtra(LightsOutPlay.NEW_GAME, true);
		startActivityForResult(intent, PLAY_GAME);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, 
			Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (doesSavedGameExist()) {
			continueGameButton.setEnabled(true);
			if (requestCode == PLAY_GAME) {
				Toast.makeText(this, R.string.game_saved, Toast.LENGTH_SHORT).show();
			}
		} else {
			continueGameButton.setEnabled(false);
		}
	}
	
	private boolean doesSavedGameExist() {
		try {
			FileInputStream is = this.openFileInput(LSerializer.SAVE_FILE_NAME);
			if (is.available() == 0) {
				return false;
			}
			if (is.read() != 1) {
				return true;
			}
		} catch (IOException e) {
		}
		
		return false;
	}
	
	private void showInstructionsDialog() {
	    FragmentManager fm = getSupportFragmentManager();
	    InstructionsDialog instructionsDialog = new InstructionsDialog();
	    instructionsDialog.show(fm, "activity_instrucciones_dialog");
	}
	
	private void showAboutDialog() {
	    FragmentManager fm = getSupportFragmentManager();
	    AboutDialog aboutDialog = new AboutDialog();
	    aboutDialog.show(fm, "activity_info_dialog");
	}
}