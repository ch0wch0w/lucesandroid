package com.example.juegoluces.ui;

import com.example.juegoluces.R;

import android.support.v4.app.DialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class AboutDialog extends DialogFragment {

	private Button bOk;

    public AboutDialog() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_info_dialog, container);
        bOk = (Button) view.findViewById(R.id.about_ok);
        getDialog().setTitle("Información");
        
        bOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

        return view;
    }
}