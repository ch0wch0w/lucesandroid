package com.example.juegoluces;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.Button;


public class Bombilla extends Button {
  private static final int ESPACIO = 3; 

  private Tablero tablero;
  private boolean isEncendida;
  private int espacioExtra = 0;
  
  MediaPlayer mp;
  
  
  final Handler handler = new Handler();
  final Runnable invalidateRunnable = new Runnable() {
    public void run() {
      invalidate();
    }
  };
  
  public Bombilla(Context contexto, Tablero tablero) {
    this(contexto, tablero, false);
  }
  
  public Bombilla(Context context, Tablero tablero, boolean isEncendida) {
    super(context);
    this.tablero = tablero;
    this.isEncendida = isEncendida;
    
    this.setFocusableInTouchMode(true);
  }
  
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    setMeasuredDimension(0, 0);
  }
  
  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    canvas.drawRect(new Rect(0, 0, getWidth(), getHeight()), new Paint());
    
    if (isFocused()) {
      drawImage(canvas, R.drawable.fondo_rojo, 0);
    }

    int imageId = isEncendida ? R.drawable.encendida : R.drawable.apagada;
    
    drawImage(canvas, imageId, ESPACIO + espacioExtra);
  }
  
  private void drawImage(Canvas canvas, int imageId, int padding) {
    int edgeSize = getWidth() - padding * 2;
    
    String key = edgeSize + "," + imageId;
    
    Bitmap bitmap = this.tablero.pieceBitmapMap.get(key);
    if (bitmap == null) {
      bitmap = BitmapFactory.decodeResource(getResources(), imageId);
      bitmap = Bitmap.createScaledBitmap(bitmap, edgeSize, edgeSize, true);
      this.tablero.pieceBitmapMap.put(key, bitmap);
    }
    
    canvas.drawBitmap(bitmap, padding, padding, null);
  }
  
  @Override
  public boolean onTouchEvent(MotionEvent event) {
    switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        this.requestFocus();
        return true;
      case MotionEvent.ACTION_UP:
        this.tablero.togglePiece(this);
        Context context = this.getContext();
        MediaPlayer mp = null;
        
        mp = MediaPlayer.create(context.getApplicationContext(), R.raw.switch_sound);
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mp.start();
        
        this.invalidate();
        return true;
    }
    return false;
  }
  
  @Override
  public boolean onKeyUp(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER
        || keyCode == KeyEvent.KEYCODE_ENTER) {
      this.tablero.togglePiece(this);
    }
    return super.onKeyUp(keyCode, event);
  }
  
  public void toggleLights() {
        
    this.isEncendida = !this.isEncendida;
    this.invalidate();
  }
  
  public boolean isLightOn() {
    return isEncendida;
  }
  

  
  

public Bombilla(Context context) {
	super(context);
}
  
}
